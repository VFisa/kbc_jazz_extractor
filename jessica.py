__author__ = 'Martin Fiser'
__credits__ = 'Martin Fiser, 2017, Twitter: @VFisa'


# Import Libraries
import os
import requests
import pandas as pd
import sys
import json
import logging
from keboola import docker


## initialize application
cfg = docker.Config('/data/')
params = cfg.get_parameters()
API_KEY = cfg.get_parameters()["token"]

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
#fileout = "/data/out/tables/data.csv"
logging.basicConfig(filename='python_job.log',level=logging.DEBUG)
filesout = {  "tasks" 		: "/data/out/tables/tasks.csv",
			  "jobs"  		: "/data/out/tables/jobs.csv",
			  "applicants" 	: "/data/out/tables/applicants.csv",
			  "hires" 		: "/data/out/tables/hires.csv"}

# End points
endpoints = { "tasks" 		: "/v1/tasks/page/",
			  "jobs"  		: "/v1/jobs/page/",
			  "applicants" 	: "/v1/applicants/page/",
			  "hires" 		: "/v1/hires/page/"}


def get_data( uri ):
    """
    Get data from resumator API. Each response should is then converted to dictionary.
    Function then returns all data as dictionary
    """

    items = 100
    page = 1
    baseurl = "http://api.resumatorapi.com"
    endpoint = uri
    domain = "resumatorapi"
    parameters = {"apikey": API_KEY}
    frames = []
    logging.info("Calling Jazz. Standby.")

    while items==100:
        #print("Querying page number: "+str(page))
        link = baseurl+endpoint+str(page)
        #print(link)
        logging.info("Querying page number: "+str(page))
        response = requests.get(link, params=parameters)
        raw = response.json()

        items = len(raw)
        #print("Items retrieved: "+str(items))
        logging.info("Items retrieved: "+str(items))
        for a in raw:
            frames.append(a)

        page += 1

    return frames

for api in endpoints.keys():
    uri = endpoints[api]
    logging.info("Retrieving %s", uri)

	## get data
    data = get_data(uri)
	## load data as pandas DF
    model = pd.DataFrame(data)
	## output DF as CSV file
    fileout = filesout[api]
    model.to_csv(fileout, index=False)

logging.info("Job done.")
